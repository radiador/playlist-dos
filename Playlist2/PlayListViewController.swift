//
//  PlayListViewController.swift
//  Playlist2
//
//  Created by formador on 9/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class PlayListViewController: UIViewController {

    @IBOutlet weak var playlistTable: UITableView!
    @IBOutlet weak var songNameTxt: UITextField!
    
    var user: User?
    
    var databaseRef: DatabaseReference?
    
    var songs = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        databaseRef = Database.database().reference()

        playlistTable.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")

        guard let databaseRef = databaseRef, let user = user else { return }

        databaseRef.child("users").child(user.uid).child("songs").observe( .value) { snapshot in
            
            let songs = snapshot.value as? NSDictionary
            
            guard let songsList = songs else { return }
            
            self.songs.removeAll()
            
            for song in songsList.allValues {
                
                let songInfo = (song as! NSDictionary)["songName"]
                
                self.songs.append(songInfo as! String)
            }
            
            
            self.playlistTable.reloadData()
        }
    }
    

    @IBAction func addButtonAction(_ sender: Any) {
        
        guard let databaseRef = databaseRef, let user = user else { return }
        
        if let songName = songNameTxt.text, songName.count > 3 {
        
            databaseRef.child("users").child(user.uid).child("songs").childByAutoId().setValue(["songName": songName])
   
        } else {
            
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            let alertController = UIAlertController(title: "Atencion", message: "La canción debe tener mas de 3 caracteres", preferredStyle: .alert)
            
            alertController.addAction(alertAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
}

extension PlayListViewController:

UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        
        cell?.textLabel?.text = songs[indexPath.row]
        
        return cell ?? UITableViewCell()
    }
    
    
    
}
