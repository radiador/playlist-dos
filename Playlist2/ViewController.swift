//
//  ViewController.swift
//  Playlist2
//
//  Created by formador on 9/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonAction(_ sender: Any) {
        
        
        let email = emailTxt.text
        let password = passwordTxt.text
        
        if let email = email, let password = password {
            
            Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
                
                if let user = authResult?.user {
                    
                    self.user = user
                    
                    print("Hay usuario")
                    self.performSegue(withIdentifier: "playListVCIdentifier", sender: nil)
                }
                
                if let error = error {
                    
                    print("Algun tipo de error al logarse \(error.localizedDescription)")
                    
                    Auth.auth().createUser(withEmail: email, password: password, completion: { authResult, error in
                        
                        if let error = error {
                            
                            print("Algun tipo de error al logarse \(error.localizedDescription)")
                        } else if let user = authResult?.user {
                            
                            self.user = user

                            self.performSegue(withIdentifier: "playListVCIdentifier", sender: nil)
                        }
                        
                    })
                }
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "playListVCIdentifier" {
            
            if let playListViewController = segue.destination as? PlayListViewController {
                
                playListViewController.user = user
            }
        }
    }
    
}

